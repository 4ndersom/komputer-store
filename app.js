// Bank
const balanceElement = document.getElementById("balance");
const outstandingLoanElement = document.getElementById("outstandingLoan");
const getALoanButton = document.getElementById("getALoan");

// Work
const salaryElement = document.getElementById("salary");

const bankButton = document.getElementById("bank");
const workButton = document.getElementById("work");
const repayLoanButton = document.getElementById("repayLoan");

// Laptops
// Laptops selection section
const laptopsDropdown = document.getElementById("laptops");
const featuresElement = document.getElementById("features");

// Laptops info and buy section

const imageElement = document.getElementById("image");

const titleElement = document.getElementById("title");
const descriptionElement = document.getElementById("description");

const priceElement = document.getElementById("price");
const buyNowButton = document.getElementById("buyNow");

// Fetching data from API

let laptopsData = [];

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
  .then((response) => response.json())
  .then((data) => (laptopsData = data));

console.log(laptopsData);

//

let outstandingLoan = 0;
let balance = 0;
let salary = 0;
let selectedLaptop = 1;
let price = 200;

const handleGetALoanButtonClick = () => {
  if (outstandingLoan === 0) {
    const message =
      "You can loan up to " +
      2 * balance +
      ". How much would you like to loan?";
    const desiredAmount = parseFloat(prompt(message, "0"));
    if (desiredAmount <= 2 * balance) {
      alert("Loan granted");
      outstandingLoan = desiredAmount;
      outstandingLoanElement.innerText = outstandingLoan;
      balance = balance + outstandingLoan;
      balanceElement.innerText = balance;
    } else {
      alert("No!!! Too much!");
    }
  } else {
    alert("You already have a loan - you better pay that one off first ;)");
  }
};

const handleBankButtonClick = () => {
  if (outstandingLoan > 0) {
    if (outstandingLoan >= 0.1 * salary) {
    outstandingLoan = outstandingLoan - 0.1 * salary;
    balance = balance + 0.9 * salary;
    alert("10 % of salary paid off on loan and the rest banked");
    } else {
      outstandingLoan = 0;
      balance = balance + salary - outstandingLoan;
      alert("Loan paid off and rest of salary banked B)");
    }
  } else {
    balance = balance + salary;
    alert("salary banked B)");
  }
  balanceElement.innerText = balance;
  salary = 0;
  salaryElement.innerText = salary;
  outstandingLoanElement.innerText = outstandingLoan;
};

const handleWorkButtonClick = () => {
  salary = salary + 100;
  salaryElement.innerText = salary;
};

const handleRepayLoanButtonClick = () => {
  if (salary > outstandingLoan) {
    balance = balance + salary - outstandingLoan;
    outstandingLoan = 0;
    alert("Loan paid off and rest of the salary banked B)");
  } else {
    outstandingLoan = outstandingLoan - salary;
    alert("Salary paid off on loan :-)");
  }
  salary = 0;
  salaryElement.innerText = salary;
  balanceElement.innerText = balance;
  outstandingLoanElement.innerText = outstandingLoan;
};

const handleBuyNowButtonClick = () => {
  if (balance >= price) {
    const buyMessage = "Congrats! You are now the proud owner of a new " + selectedLaptop.title + "!";
    alert(buyMessage);
    balance = balance - price;
    balanceElement.innerText = balance;
  } else {
    alert("Insufficient funds :(");
  }
};

const handleLaptopsDropdownChange = (e) => {
  console.log(e);
  selectedLaptop = laptopsData[e.target.selectedIndex];
  priceElement.innerText = selectedLaptop.price;
  featuresElement.innerText = selectedLaptop.specs.join(", ");
  titleElement.innerText = selectedLaptop.title;
  descriptionElement.innerText = selectedLaptop.description;
  price = selectedLaptop.price;
  imageElement.src = "https://noroff-komputer-store-api.herokuapp.com/"+selectedLaptop.image
};

laptopsDropdown.addEventListener("change", handleLaptopsDropdownChange)
getALoanButton.addEventListener("click", handleGetALoanButtonClick);
bankButton.addEventListener("click", handleBankButtonClick);
workButton.addEventListener("click", handleWorkButtonClick);
repayLoanButton.addEventListener("click", handleRepayLoanButtonClick);
buyNowButton.addEventListener("click", handleBuyNowButtonClick);